#!/usr/bin/sh -eu

interactive=0

while getopts i opt; do
    case $opt in
	i) interactive=1 ;;
	*) echo "Option not recognised"; exit 1 ;;
    esac
done

echo "Using kernel dir" ${KERNEL_SRC:=~/kernel/linux}

rm transport.{in,out} || true
mkfifo transport.in transport.out

if [ ! -e swap.qcow2 ]; then
    qemu-img create -f qcow2 swap.qcow2 4G
fi

		       # -object memory-backend-ram,size=2G,id=m0 \
		       # -object memory-backend-ram,size=2G,id=m1 \
		       # -numa node,nodeid=0,memdev=m0 \
		       # -numa node,nodeid=1,memdev=m1 \
		       # -numa cpu,node-id=0,socket-id=0 \
		       # -numa cpu,node-id=1,socket-id=1 \
		       
		       # dyndbg=\"file drivers/base/bus.c +pf\"

if [ $interactive -eq 1 ]; then
    echo 'Starting QEMU interactively'
    stty intr ^]
    qemu-system-x86_64 -enable-kvm -m 8G \
                       -cpu host \
		       -smp 4 \
		       -display none \
		       -nodefaults \
		       -device virtio-rng-pci \
		       -device virtio-serial \
		       -chardev stdio,id=tty,mux=on,logfile=tty.log \
		       -device virtconsole,chardev=tty \
		       -serial chardev:tty \
		       -chardev pipe,id=transport,path=transport,logfile=transport.log \
		       -device virtserialport,chardev=transport \
		       -drive if=virtio,file=swap.qcow2,cache=unsafe \
		       -kernel $KERNEL_SRC/arch/x86/boot/bzImage \
		       -initrd myinitrd \
		       -virtfs local,path=$(realpath ~),mount_tag=host,security_model=mapped-xattr,readonly=on \
		       -append "rd.break=cmdline rd.systemd.unit=dracut-cmdline.service console=hvc0 earlyprintk=ttyS0,115200 ignore_loglevel no_hash_pointers slub_debug=FUZ,buffer_head page_owner=on"

    exit 0
fi

echo 'Starting QEMU in background'
qemu-system-x86_64 -enable-kvm -m 8G -smp 8 -display none \
		   -nodefaults \
		   -daemonize \
		   -pidfile qemu.pid \
		   -device virtio-rng-pci \
		   -device virtio-serial \
		   -chardev null,id=tty,logfile=tty.log \
		   -device virtconsole,chardev=tty \
		   -chardev pipe,id=transport,path=transport,logfile=transport.log \
		   -device virtserialport,chardev=transport \
		   -kernel $KERNEL_SRC/arch/x86/boot/bzImage \
		   -initrd myinitrd \
		   -append "rd.systemd.unit=emergency rd.shell=1 console=hvc0"

trap "kill $(cat qemu.pid)" 0 1 3 6 9 15
