#!/usr/bin/sh -eu
# Largely copied from rapido

echo "Using kernel dir" ${KERNEL_SRC:=~/kernel/linux}
echo "Using LTP dir"  ${LTP_DIR:=/opt/ltp}
echo "Using runtest file" ${LTP_RUNTEST:=../../test/runtest-file}

kver="$(cat ${KERNEL_SRC}/include/config/kernel.release)"
test_cases=$(cat $LTP_RUNTEST | awk "{ FS = \" \"; print \"$LTP_DIR/testcases/bin/\"\$2 }")

echo "Using kmoddir" ${KERNEL_MODS:=$KERNEL_SRC/mods/lib/modules/$kver}

drivers=$(find $KERNEL_MODS -iname "*.ko" -printf "%f\n" | sed s/.ko//)

mkdir initrds || true

dracut \
    --no-compress --no-hostonly --no-early-microcode --no-hostonly-cmdline \
    --force --tmpdir initrds \
    --kver $kver \
    --kmoddir $KERNEL_MODS \
    --drivers "$drivers" \
    --modules "base dash systemd systemd-initrd dracut-systemd" \
    --install "strace grep id su useradd head sh ip mkswap swapon bpftrace rmdir llvm-symbolizer mktemp" \
    --install "mkfs.btrfs mkfs.exfat mkfs.xfs mkfs.ntfs mkfs.ext4 mkfs.ext2 mount.ntfs mkswap dd zcat gzip basename killall awk find chmod seq cut tr sort stat losetup fallocate btrfs fish" \
    --install "tac wc tc resize locale" \
    --install "$KERNEL_SRC/tools/perf/perf" \
    --install "$test_cases" \
    --include ../executor /usr/bin/executor \
    --include ../../test/profile.sh /.profile \
    myinitrd
