#!/bin/bash

echo 'Running .profile'

export TERM=linux
resize

# mkswap /dev/vda
# swapon /dev/vda

mount -t tracefs nodev /sys/kernel/tracing
mount -t debugfs nodev /sys/kernel/debug

mkdir /mnt
mount -t 9p -o trans=virtio host /mnt

fallocate -l 300MiB /tmp/btrfs.img
losetup -f /tmp/btrfs.img
mkfs.btrfs /dev/loop0
mkdir /tmp/btrfs
mount -t btrfs /dev/loop0 /tmp/btrfs

export PATH=$PATH:/home/rich/qa/ltp-install/testcases/bin:/mnt/qa/ltp-install/testcases/bin
export LTPROOT=/mnt/qa/ltp-install

# echo '0' > /sys/kernel/tracing/ftrace_enabled
# echo '*cgroup* do_sys_openat2:traceon' > /sys/kernel/tracing/set_ftrace_filter

