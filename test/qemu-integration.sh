#!/usr/bin/sh -eu

echo "Using runtest file" ${LTP_RUNTEST:=../../test/runtest-file}

. ../test/push-test-dir.sh

. ../../test/qemu-dracut.sh
. ../../test/qemu-start.sh

# ../../tstctl init
# ../../tstctl set NPROC 1
# ../../tstctl set TEST_TIMEOUT 10
# ../../tstctl add-tests $LTP_RUNTEST

# echo "Running tests with one worker"
# time {
# while [ $(../../tstctl status) = TODO ]; do
#     echo "More tests; (re)starting driver"
#     ../driver >transport.in <transport.out
# done
# }

echo "Running test with 4 workers"
../../tstctl init
../../tstctl set NPROC 4
../../tstctl set TEST_TIMEOUT 10
../../tstctl add-tests $LTP_RUNTEST

time {
while [ $(../../tstctl status) = TODO ]; do
    echo "More tests; (re)starting driver"
    ../driver >transport.in <transport.out
done
}

echo "All done!"

# expect ../../test/tester-concurrent.exp ${TEST_DIR}/transport || {
#     echo -e "Final SUT console output was:\n"
#     tail tty.log
#     exit 123
# }

# . ../../test/pop-test-dir.sh
